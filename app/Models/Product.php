<?php

namespace App\Models;

use Ecommerce\Common\Containers\Product\CategoryContainer;
use Ecommerce\Common\Containers\Product\ProductContainer;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getPriceFormattedAttribute(): string
    {
        return '$' . number_format($this->price, 2);
    }

    public function toContainer(): ProductContainer
    {
        return new ProductContainer(
            $this->id,
            $this->name,
            $this->description,
            $this->price,
            new CategoryContainer(
                /** @phpstan-ignore-next-line */
                $this->category->id,
                /** @phpstan-ignore-next-line */
                $this->category->name
            )
        );
    }
}
