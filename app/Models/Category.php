<?php

namespace App\Models;

use Ecommerce\Common\Containers\Product\CategoryContainer;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function toContainer(): CategoryContainer
    {
        return new CategoryContainer($this->id, $this->name);
    }
}
