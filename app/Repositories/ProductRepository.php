<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class ProductRepository
{
    public function create(Category $category, string $name, string $description, float $price): Product
    {
        return Product::create([
            'category_id' => $category->id,
            'name' => $name,
            'description' => $description,
            'price' => $price
        ]);
    }

    /**
     * @return Collection<Product>
     */
    public function search(
        ?string $searchTerm,
        ?string $sortBy,
        ?string $sortDirection
    ): Collection {
        return Product::select('products.*')
            ->when($searchTerm, fn (Builder $query) =>
                $query
                    ->leftJoin('categories', 'categories.id', 'products.category_id')
                    ->where('products.name', 'LIKE', "%$searchTerm%")
                    ->orWhere('products.description', 'LIKE', "%$searchTerm%")
                    ->orWhere('categories.name', 'LIKE', "%$searchTerm%")
            )->when($sortBy, fn (Builder $query) =>
                $query->orderBy("products.$sortBy", $sortDirection)
            )->get();
    }
}
