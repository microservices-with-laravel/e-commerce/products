<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Support\Collection;

class CategoryRepository
{
    public function findOrFail(int $id): Category
    {
        return Category::findOrFail($id);
    }

    /**
     * @return Collection<Category>
     */
    public function getAll(): Collection
    {
        return Category::all();
    }

    public function create(string $name): Category
    {
        return Category::create([
            'name' => $name,
        ]);
    }
}
