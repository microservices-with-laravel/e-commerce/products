<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoryRequest;
use App\Services\CategoryService;

class CategoryController extends Controller
{
    public function __construct(private CategoryService $categoryService)
    {
    }

    public function index()
    {
        return [
            'data' => $this->categoryService->getAll()
        ];
    }

    public function store(StoreCategoryRequest $request)
    {
        return [
            'data' => $this->categoryService->create($request->getName())->toContainer(),
        ];
    }
}
