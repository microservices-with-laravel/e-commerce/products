<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetProductsRequest;
use App\Http\Requests\StoreProductRequest;
use App\Models\Product;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Services\ProductService;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    public function __construct(
        private ProductService $productService,
        private CategoryRepository $categories
    ) {
    }

    public function index(GetProductsRequest $request)
    {
        $products = $this->productService->search(
            $request->getSearchTerm(),
            $request->getSortBy(),
            $request->getSortDirection()
        );

        return [
            'data' => $products
        ];
    }

    public function get(Product $product)
    {
        return ['data' => $product->toContainer()];
    }

    public function store(StoreProductRequest $request)
    {
        $category = $this->categories->findOrFail(
            $request->getCategoryId()
        );

        $product = $this->productService->create(
            $category,
            $request->getName(),
            $request->getDescription(),
            $request->getPrice()
        );

        return response([
            'data' => $product->toContainer()
        ], Response::HTTP_CREATED);
    }
}
