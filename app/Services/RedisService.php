<?php

namespace App\Services;

use Ecommerce\Common\Containers\Product\ProductContainer;
use Ecommerce\Common\Events\Product\ProductCreatedEvent;
use Ecommerce\Common\Services\RedisService as BaseRedisService;

class RedisService extends BaseRedisService
{
    public function getServiceName(): string
    {
        return 'products';
    }

    public function publishProductCreated(ProductContainer $productContainer): void
    {
        $this->publish(new ProductCreatedEvent($productContainer));
    }
}
