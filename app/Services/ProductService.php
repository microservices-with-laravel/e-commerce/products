<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Ecommerce\Common\Containers\Product\ProductContainer;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ProductService
{
    public function __construct(private ProductRepository $products, private RedisService $redisService)
    {
    }

    public function create(
        Category $category,
        string $name,
        string $description,
        float $price
    ): Product {
        return DB::transaction(function () use ($category, $name, $description, $price) {
            $product = $this->products->create(
                $category,
                $name,
                $description,
                $price
            );

            $this->redisService->publishProductCreated(
                $product->toContainer()
            );

            return $product;
        });
    }

    /**
     * @param string|null $searchTerm
     * @param string|null $sortBy
     * @param string|null $sortDirection
     * @return Collection<ProductContainer>
     */
    public function search(
        ?string $searchTerm,
        ?string $sortBy,
        ?string $sortDirection
    ): Collection {
        return $this->products
            ->search($searchTerm, $sortBy, $sortDirection)
            ->map
            ->toContainer();
    }
}
