<?php

namespace App\Services;

use App\Models\Category;
use App\Repositories\CategoryRepository;
use Ecommerce\Common\Containers\Product\CategoryContainer;
use Illuminate\Support\Collection;

class CategoryService
{
    public function __construct(private CategoryRepository $categories)
    {
    }

    /**
     * @return Collection<CategoryContainer>
     */
    public function getAll(): Collection
    {
        return $this->categories
            ->getAll()
            ->map
            ->toContainer();
    }

    public function create(string $name): Category
    {
        return $this->categories->create($name);
    }
}
